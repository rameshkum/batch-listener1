package com.customer.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.customer.kafka.consumer.Listener;

@SpringBootApplication
@EnableScheduling
public class SpringKafkaListener1 {
	private static final Logger LOG = LoggerFactory.getLogger(Listener.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringKafkaListener1.class, args);
	}

}