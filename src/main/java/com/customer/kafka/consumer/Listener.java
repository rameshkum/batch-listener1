package com.customer.kafka.consumer;

import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.customer.kafka.model.Employee;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Listener {


    private static final Logger LOG = LoggerFactory.getLogger(Listener.class);

    @KafkaListener(id = "batch-listener1",groupId = "group-id-test",
    	        topicPartitions = {
    	                @TopicPartition(topic = "${app.topic.batch}",
    	                        partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))
    	        },
    	        containerFactory = "kafkaListenerContainerFactory")
    public void receive(@Payload String message,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partitionId,
            @Header(KafkaHeaders.OFFSET) Long offset, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) throws Exception {
    	
    	LOG.info("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        LOG.info(" batch-listener1 --partitionId  " +partitionId);
        LOG.info(" batch-listener1 --offset  " +offset);
        LOG.info("beginning to consume batch messages" + message);
        LOG.info("received message='{}' with partition-offset='{}'",
        		message, partitionId + "-" + offset);
        
		
		ICsvBeanWriter beanWriter = null;
		CellProcessor[] processors = new CellProcessor[] { 
				new NotNull(), // id
				new NotNull(), // firstName
				new NotNull() }; //lastName

		try {
			beanWriter = new CsvBeanWriter(new FileWriter("outputData.csv"), 
			CsvPreference.STANDARD_PREFERENCE);
			String[] header = { "id", "firstName", "lastName" };
			beanWriter.writeHeader(header);

			JsonNode idjsonNode = getObjectMapper().readTree(message);
			String id = idjsonNode.get("id").asText();

			JsonNode firstNamejsonNode = getObjectMapper().readTree(message);
			String firstName = firstNamejsonNode.get("firstName").asText();

			JsonNode lastNamejsonNode = getObjectMapper().readTree(message);
			String lastName = lastNamejsonNode.get("lastName").asText();

			Employee employeeNew = new Employee(id, firstName, lastName);

			beanWriter.write(employeeNew, header, processors);

		} catch (IOException ex) {
			System.err.println("Error writing the CSV file: " + ex);
		} finally {
			if (beanWriter != null) {
				try {
					beanWriter.close();
				} catch (IOException ex) {
					System.err.println("Error closing the writer: " + ex);
				}
			}
		}
     
    } 
    
	@Bean
	public ObjectMapper getObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		return objectMapper;
	}
}
    
    
